import SkullGame from "../../games/skull-client";

window.customElements.define("skull-game", SkullGame);

import main from "./Main.elm";

let model;
try {
  model = JSON.parse(localStorage.getItem("model"));
} catch (e) {
  console.warn(e);
}

let app = main.Elm.Main.init({
  node: document.getElementById("elm"),
  flags: model
});

app.ports.modelJsonChanged.subscribe(model => {
  localStorage.setItem("model", JSON.stringify(model));
});
