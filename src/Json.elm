module Json exposing (InitialStateReq, PerformActionReq, PerformActionRes, jsonDecInitialStateReq, jsonDecPerformActionReq, jsonDecPerformActionRes, jsonEncInitialStateReq, jsonEncPerformActionReq, jsonEncPerformActionRes)

import Json.Decode as D
import Json.Encode as E
import Json.Helpers as H


type alias InitialStateReq =
    { players : List Int
    , settings : E.Value
    , seed : Int
    }


jsonDecInitialStateReq : D.Decoder InitialStateReq
jsonDecInitialStateReq =
    D.succeed (\pplayers psettings pseed -> { players = pplayers, settings = psettings, seed = pseed })
        |> H.required "players" (D.list D.int)
        |> H.required "settings" D.value
        |> H.required "seed" D.int


jsonEncInitialStateReq : InitialStateReq -> E.Value
jsonEncInitialStateReq val =
    E.object
        [ ( "players", E.list E.int val.players )
        , ( "settings", val.settings )
        , ( "seed", E.int val.seed )
        ]


type alias PerformActionReq =
    { performedBy : Int
    , action : E.Value
    , state : E.Value
    , seed : Int
    }


jsonDecPerformActionReq : D.Decoder PerformActionReq
jsonDecPerformActionReq =
    D.succeed (\pperformedBy paction pstate pseed -> { performedBy = pperformedBy, action = paction, state = pstate, seed = pseed })
        |> H.required "performedBy" D.int
        |> H.required "action" D.value
        |> H.required "state" D.value
        |> H.required "seed" D.int


jsonEncPerformActionReq : PerformActionReq -> E.Value
jsonEncPerformActionReq val =
    E.object
        [ ( "performedBy", E.int val.performedBy )
        , ( "action", val.action )
        , ( "state", val.state )
        , ( "seed", E.int val.seed )
        ]


type alias PerformActionRes =
    { completed : Bool
    , nextState : E.Value
    }


jsonDecPerformActionRes : D.Decoder PerformActionRes
jsonDecPerformActionRes =
    D.succeed (\pcompleted pnextState -> { completed = pcompleted, nextState = pnextState })
        |> H.required "completed" D.bool
        |> H.required "nextState" D.value


jsonEncPerformActionRes : PerformActionRes -> E.Value
jsonEncPerformActionRes val =
    E.object
        [ ( "completed", E.bool val.completed )
        , ( "nextState", val.nextState )
        ]
