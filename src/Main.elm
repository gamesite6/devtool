port module Main exposing (main)

import Browser
import Dict exposing (Dict)
import Html exposing (Html, aside, button, div, fieldset, input, label, legend, node, p, text, textarea)
import Html.Attributes exposing (attribute, checked, class, disabled, for, id, name, property, type_, value)
import Html.Events exposing (on, onClick, onInput)
import Http
import Json
import Json.Decode as D
import Json.Encode as E
import Json.Helpers exposing (jsonDecDict, jsonEncDict)


main : Program D.Value Model Msg
main =
    Browser.document
        { init = init
        , subscriptions = subscriptions
        , update = update
        , view = view
        }


type alias UserId =
    Int


type alias User =
    { id : Int
    , name : String
    }


type alias Users =
    Dict UserId User


jsonEncUser : User -> E.Value
jsonEncUser user =
    E.object
        [ ( "id", E.int user.id )
        , ( "name", E.string user.name )
        ]


jsonDecUser : D.Decoder User
jsonDecUser =
    D.map2 User
        (D.field "id" D.int)
        (D.field "name" D.string)


jsonEncUsers : Users -> E.Value
jsonEncUsers =
    jsonEncDict E.int jsonEncUser


jsonDecUsers : D.Decoder Users
jsonDecUsers =
    jsonDecDict D.int jsonDecUser


type alias Model =
    { userId : Maybe UserId
    , users : Dict UserId User
    , settings : String
    , settingsInput : String
    , state : String
    , stateInput : String
    }


jsonEncModel : Model -> E.Value
jsonEncModel model =
    E.object
        [ ( "userId", Maybe.withDefault E.null (Maybe.map E.int model.userId) )
        , ( "users", jsonEncUsers model.users )
        , ( "settings", E.string model.settings )
        , ( "settingsInput", E.string model.settingsInput )
        , ( "state", E.string model.state )
        , ( "stateInput", E.string model.stateInput )
        ]


jsonDecModel : D.Decoder Model
jsonDecModel =
    D.map6
        Model
        (D.maybe (D.field "userId" D.int))
        (D.field "users" jsonDecUsers)
        (D.field "settings" D.string)
        (D.field "settingsInput" D.string)
        (D.field "state" D.string)
        (D.field "stateInput" D.string)


init : D.Value -> ( Model, Cmd Msg )
init flags =
    case D.decodeValue jsonDecModel flags of
        Ok model ->
            ( model, Cmd.none )

        Err _ ->
            ( { userId = Nothing
              , users =
                    Dict.fromList
                        [ ( 111, { id = 111, name = "john_111" } )
                        , ( 222, { id = 222, name = "emma_222" } )
                        , ( 333, { id = 333, name = "william_333" } )
                        , ( 444, { id = 444, name = "olivia_444" } )
                        ]
              , settings = "[3,4,5,6]"
              , settingsInput = "[3,4,5,6]"
              , state = ""
              , stateInput = ""
              }
            , Cmd.none
            )


port modelJsonChanged : E.Value -> Cmd msg


modelChanged : Model -> Cmd msg
modelChanged model =
    modelJsonChanged (jsonEncModel model)


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none


type Input
    = SettingsInput
    | StateInput


type Msg
    = RequestInitialState
    | InitialStateReceived (Result Http.Error E.Value)
    | SetUserId (Maybe UserId)
    | Apply Input
    | Change Input String
    | Reset Input
    | PerformAction E.Value
    | ActionResponseReceived (Result Http.Error Json.PerformActionRes)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        RequestInitialState ->
            ( model
            , Http.post
                { url = "/initial-state"
                , body =
                    Http.jsonBody
                        (Json.jsonEncInitialStateReq
                            { players = [ 111, 222, 333, 444 ]
                            , settings = E.list E.int [ 3, 4, 5, 6 ]
                            , seed = 42
                            }
                        )
                , expect = Http.expectJson InitialStateReceived D.value
                }
            )

        InitialStateReceived (Ok nextStateJson) ->
            let
                nextState =
                    E.encode 2 nextStateJson

                nextModel =
                    { model | state = nextState, stateInput = nextState }
            in
            ( nextModel, modelChanged nextModel )

        InitialStateReceived (Err _) ->
            ( model, Cmd.none )

        SetUserId userId ->
            let
                nextModel =
                    { model | userId = userId }
            in
            ( nextModel, modelChanged nextModel )

        Change SettingsInput value ->
            let
                nextModel =
                    { model | settingsInput = value }
            in
            ( nextModel, modelChanged nextModel )

        Change StateInput value ->
            let
                nextModel =
                    { model | stateInput = value }
            in
            ( nextModel, modelChanged nextModel )

        Apply SettingsInput ->
            let
                nextModel =
                    { model | settings = model.settingsInput }
            in
            ( nextModel, modelChanged nextModel )

        Apply StateInput ->
            let
                nextModel =
                    { model | state = model.stateInput }
            in
            ( nextModel, modelChanged nextModel )

        Reset SettingsInput ->
            let
                nextModel =
                    { model | settingsInput = model.settings }
            in
            ( nextModel, modelChanged nextModel )

        Reset StateInput ->
            let
                nextModel =
                    { model | stateInput = model.state }
            in
            ( nextModel, modelChanged nextModel )

        PerformAction actionJson ->
            case ( model.userId, D.decodeString D.value model.state ) of
                ( Just userId, Ok stateJson ) ->
                    ( model
                    , Http.post
                        { url = "/perform-action"
                        , body =
                            Http.jsonBody
                                (Json.jsonEncPerformActionReq
                                    { performedBy = userId
                                    , action = actionJson
                                    , state = stateJson
                                    , seed = 42
                                    }
                                )
                        , expect = Http.expectJson ActionResponseReceived Json.jsonDecPerformActionRes
                        }
                    )

                ( _, _ ) ->
                    ( model, Cmd.none )

        ActionResponseReceived (Ok res) ->
            let
                nextState =
                    E.encode 2 res.nextState

                nextModel =
                    { model | state = nextState, stateInput = nextState }
            in
            ( nextModel, modelChanged nextModel )

        ActionResponseReceived (Err err) ->
            Debug.log
                (Debug.toString err)
                ( model, Cmd.none )


view : Model -> Browser.Document Msg
view model =
    { title = "Devtool - Gamesite6"
    , body =
        [ div [ class "devtool-game" ] [ gameView model ]
        , aside [ class "devtool-aside" ]
            [ div [ class "panel shadow border", class "reference" ] []
            , devtoolControls model
            ]
        ]
    }


devtoolControls : Model -> Html Msg
devtoolControls model =
    div []
        [ button [ onClick RequestInitialState ] [ text "new game" ]
        , fieldset []
            [ legend [] [ text "user" ]
            , input
                [ onClick (SetUserId Nothing)
                , checked (model.userId == Nothing)
                , type_ "radio"
                , id "user-none"
                , name "user"
                , value ""
                ]
                []
            , label [ for "user-none" ] [ text "none" ]
            , input
                [ onClick (SetUserId (Just 111))
                , checked (model.userId == Just 111)
                , type_ "radio"
                , id "user-111"
                , name "user"
                , value ""
                ]
                []
            , label [ for "user-111" ] [ text "111" ]
            , input
                [ onClick (SetUserId (Just 222))
                , checked (model.userId == Just 222)
                , type_ "radio"
                , id "user-222"
                , name "user"
                , value ""
                ]
                []
            , label [ for "user-222" ] [ text "222" ]
            , input
                [ onClick (SetUserId (Just 333))
                , checked (model.userId == Just 333)
                , type_ "radio"
                , id "user-333"
                , name "user"
                , value ""
                ]
                []
            , label [ for "user-333" ] [ text "333" ]
            , input
                [ onClick (SetUserId (Just 444))
                , checked (model.userId == Just 444)
                , type_ "radio"
                , id "user-444"
                , name "user"
                , value ""
                ]
                []
            , label [ for "user-444" ] [ text "444" ]
            ]
        , fieldset []
            [ legend [] [ label [ for "settings-input" ] [ text "settings" ] ]
            , textarea [ id "settings-input", value model.settingsInput, onInput (Change SettingsInput) ] []
            , div []
                [ button
                    [ disabled (model.settings == model.settingsInput)
                    , onClick (Apply SettingsInput)
                    ]
                    [ text "apply" ]
                , button
                    [ disabled (model.settings == model.settingsInput)
                    , onClick (Reset SettingsInput)
                    ]
                    [ text "reset" ]
                ]
            ]
        , fieldset []
            [ legend [] [ label [ for "state-input" ] [ text "state" ] ]
            , textarea [ id "state-input", value model.stateInput, onInput (Change StateInput) ] []
            , div []
                [ button
                    [ disabled (model.state == model.stateInput)
                    , onClick (Apply StateInput)
                    ]
                    [ text "apply" ]
                , button
                    [ disabled (model.state == model.stateInput)
                    , onClick (Reset StateInput)
                    ]
                    [ text "reset" ]
                ]
            ]
        ]


gameView : Model -> Html.Html Msg
gameView model =
    case ( D.decodeString D.value model.settings, D.decodeString D.value model.state ) of
        ( Err err1, Err err2 ) ->
            div []
                [ p [] [ text (D.errorToString err1) ]
                , p [] [ text (D.errorToString err2) ]
                ]

        ( Err err, _ ) ->
            text (D.errorToString err)

        ( _, Err err ) ->
            text (D.errorToString err)

        ( Ok settings, Ok state ) ->
            node "skull-game"
                [ on "action" (D.map PerformAction (D.field "detail" D.value))
                , property "users" (jsonEncUsers model.users)
                , property "state" state
                , property "settings" settings
                , attribute "user-id" (Maybe.withDefault "" (Maybe.map String.fromInt model.userId))
                ]
                []
