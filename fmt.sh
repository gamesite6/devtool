#!/bin/bash

elm-format --yes src/**.elm
npx prettier --write src/**.js
